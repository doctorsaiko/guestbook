##################
# Variables
##################

DOCKER_COMPOSE = docker-compose -f ./docker/docker-compose.yml --env-file ./docker/.env
DOCKER_COMPOSE_PHP_FPM_EXEC = ${DOCKER_COMPOSE} exec -u www-data php-fpm

##################
# Docker compose
##################

build:
	${DOCKER_COMPOSE} build

start:
	${DOCKER_COMPOSE} start

stop:
	${DOCKER_COMPOSE} stop

up:
	${DOCKER_COMPOSE} up -d

logs:
	${DOCKER_COMPOSE} logs -f

down:
	${DOCKER_COMPOSE} down --remove-orphans

down-clear:
	${DOCKER_COMPOSE} down -v --remove-orphans

##################
# App
##################

bash:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bash


##################
# Database
##################
migrate-dev:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console doctrine:migrations:migrate --no-interaction
migrate-test:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console doctrine:migrations:migrate --no-interaction --env=test
migrate-diff:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console doctrine:migrations:diff --no-interaction
migration:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console make:migration --no-interaction

messenger-consume:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/console messenger:consume async

test:
	${DOCKER_COMPOSE} exec -u www-data php-fpm bin/phpunit

yarn-install:
	${DOCKER_COMPOSE} run --rm node yarn install