<?php

namespace App\Comment\Bus\SendAnswerToComment;

use App\Comment\Entity\Comment\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Mime\Email;
use Symfony\Component\String\UnicodeString;

#[AsMessageHandler]
class Handler
{
    public function __construct(
        private MailerInterface $mailer,
        private LoggerInterface $logger,
        private CommentRepository $commentRepository,
        private EntityManagerInterface $em,
        private MessageBusInterface $bus
    ) {
    }

    public function __invoke(Message $message): void
    {
        $text = new UnicodeString($message->getText());
        $preparedText = $text->truncate(200, '...')->toString();

        try {
            $comment = $this->commentRepository->getById($message->getCommentId());
        } catch (\DomainException $e) {
            $this->logger->log(LogLevel::ERROR, $e->getMessage());

            return;
        }

        if ($lastSendTime = $comment->getLastNotificationSendAt()) {
            $interval = (new \DateTime())->modify('-5 minutes');
            if ($interval < $lastSendTime) {
                $this->bus->dispatch($message, [new DelayStamp(5 * 60000)]);

                return;
            }
        }

        $mail = (new Email())
            ->to($message->getEmail()->getValue())
            ->subject('You got new answer on comment '.$comment->getId())
            ->text($preparedText);

        try {
            $this->mailer->send($mail);
        } catch (TransportExceptionInterface $e) {
            $this->logger->log(LogLevel::ERROR, $e->getMessage());

            return;
        }

        $comment->setLastNotificationSendAt();
        $this->em->flush();
    }
}
