<?php

namespace App\Comment\Bus\SendAnswerToComment;

use App\Comment\Entity\Comment\Email;

class Message
{
    public function __construct(private Email $email, private string $text, private int $commentId)
    {
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getCommentId(): int
    {
        return $this->commentId;
    }
}
