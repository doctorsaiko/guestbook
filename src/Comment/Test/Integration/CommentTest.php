<?php

namespace App\Comment\Test\Integration;

use App\Comment\Entity\Comment\CommentRepository;
use App\Comment\UseCase\AddComment;
use App\Tests\Integration\IntegrationTestCase;

class CommentTest extends IntegrationTestCase
{
    private CommentRepository $commentRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->commentRepository = self::$container->get(CommentRepository::class);
    }

    public function testAdd(): void
    {
        $command = new AddComment\Command();
        $command->text = 'test comment';

        $handler = self::$container->get(AddComment\Handler::class);
        $handler->handle($command);

        $id = $this->em->getConnection()->lastInsertId('comments_id_seq');
        $comment = $this->commentRepository->getById($id);

        self::assertEquals($command->text, $comment->getText());
        self::assertNull($comment->getEmail());
        self::assertNull($comment->getAvatar());
    }
}
