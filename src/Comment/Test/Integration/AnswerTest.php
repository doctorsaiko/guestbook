<?php

namespace App\Comment\Test\Integration;

use App\Comment\Entity\Comment\Comment;
use App\Comment\Entity\Comment\CommentRepository;
use App\Comment\UseCase\AddAnswer;
use App\Comment\UseCase\AddComment;
use App\Tests\Integration\IntegrationTestCase;

class AnswerTest extends IntegrationTestCase
{
    private CommentRepository $commentRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->commentRepository = self::$container->get(CommentRepository::class);
    }

    public function testAdd(): void
    {
        /**
         * TODO: replace with fixtures.
         */
        $comment = $this->createComment();

        $command = new AddAnswer\Command();
        $command->text = 'test answer';
        $command->parentId = $comment->getId();

        $handler = self::$container->get(AddAnswer\Handler::class);
        $handler->handle($command);

        $id = $this->em->getConnection()->lastInsertId('comments_id_seq');
        $comment = $this->commentRepository->getById($id);

        self::assertEquals($command->text, $comment->getText());
        self::assertNull($comment->getEmail());
        self::assertNull($comment->getAvatar());
    }

    public function testParentNotFoundError(): void
    {
        $command = new AddAnswer\Command();
        $command->text = 'test answer';
        $command->parentId = 0;

        $handler = self::$container->get(AddAnswer\Handler::class);

        $this->expectExceptionMessage('Comment not found!');

        $handler->handle($command);
    }

    public function testAnswerToAnswerError(): void
    {
        /**
         * TODO: replace with fixtures.
         */
        $answer = $this->createAnswer();

        $command = new AddAnswer\Command();
        $command->text = 'test answer';
        $command->parentId = $answer->getId();

        $handler = self::$container->get(AddAnswer\Handler::class);

        $this->expectExceptionMessage('you cannot make answer to answer, please use citation!');

        $handler->handle($command);
    }

    public function testCommentIsHiddenError(): void
    {
        /**
         * TODO: replace with fixtures.
         */
        $comment = $this->createComment();
        $comment->setIsVisible(false);

        $command = new AddAnswer\Command();
        $command->text = 'test answer';
        $command->parentId = $comment->getId();

        $handler = self::$container->get(AddAnswer\Handler::class);

        $this->expectExceptionMessage('sorry, comment is hidden!');

        $handler->handle($command);
    }

    private function createComment(): Comment
    {
        $command = new AddComment\Command();
        $command->text = 'test comment';

        $handler = self::$container->get(AddComment\Handler::class);
        $handler->handle($command);

        $id = $this->em->getConnection()->lastInsertId('comments_id_seq');

        return $this->commentRepository->getById($id);
    }

    private function createAnswer(): Comment
    {
        /**
         * TODO: replace with fixtures.
         */
        $comment = $this->createComment();

        $command = new AddAnswer\Command();
        $command->text = 'test answer';
        $command->parentId = $comment->getId();

        $handler = self::$container->get(AddAnswer\Handler::class);
        $handler->handle($command);

        $id = $this->em->getConnection()->lastInsertId('comments_id_seq');

        return $this->commentRepository->getById($id);
    }
}
