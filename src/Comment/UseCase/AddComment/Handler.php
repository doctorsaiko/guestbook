<?php

namespace App\Comment\UseCase\AddComment;

use App\Comment\Entity\Comment\Comment;
use App\Comment\Entity\Comment\Email;
use Doctrine\ORM\EntityManagerInterface;

class Handler
{
    public function __construct(
        private EntityManagerInterface $em,
    ) {
    }

    public function handle(Command $command): void
    {
        $email = $command->email ? new Email($command->email) : null;
        $comment = Comment::createComment($command->text, $email, $command->avatarPath);

        $this->em->persist($comment);
        $this->em->flush();
    }
}
