<?php

namespace App\Comment\UseCase\AddAnswer;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    #[Assert\Email]
    #[Assert\Length(max: 255)]
    public ?string $email = null;

    #[Assert\NotBlank]
    #[Assert\Length(min: 5, max: 65000)]
    public string $text = '';

    #[Assert\Length(min: 10, max: 255)]
    public ?string $avatarPath = null;

    #[Assert\Image(maxSize: '2M', minWidth: 400, maxWidth: 400, maxHeight: 400, minHeight: 400)]
    public ?UploadedFile $avatar = null;

    #[Assert\NotBlank]
    #[Assert\Type(type: 'integer', message: 'The value {{ value }} is not a valid {{ type }}.')]
    #[Assert\Range(min: 1)]
    public int $parentId;
}
