<?php

namespace App\Comment\UseCase\AddAnswer;

use App\Comment\Bus\SendAnswerToComment;
use App\Comment\Entity\Comment\Comment;
use App\Comment\Entity\Comment\CommentRepository;
use App\Comment\Entity\Comment\Email;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class Handler
{
    public function __construct(
        private EntityManagerInterface $em,
        private CommentRepository $commentRepository,
        private MessageBusInterface $bus
    ) {
    }

    public function handle(Command $command): void
    {
        $email = $command->email ? new Email($command->email) : null;

        if ($parent = $command->parentId || is_numeric($command->parentId)) {
            $parent = $this->commentRepository->getById($command->parentId);
        }

        $answer = Comment::createAnswer($command->text, $email, $command->avatarPath, $parent);

        $this->em->persist($answer);
        $this->em->flush();

        if ($answer->getParent()->getEmail()) {
            $this->bus->dispatch(
                new SendAnswerToComment\Message(
                    $answer->getParent()->getEmail(),
                    $answer->getText(),
                    $answer->getParent()->getId()
                )
            );
        }
    }
}
