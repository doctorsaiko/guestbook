<?php

namespace App\Comment\Query\GetAllComments;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class Fetcher
{
    public function __construct(
        private Connection $connection,
        private PaginatorInterface $paginator
    ) {
    }

    /**
     * @param Query $query
     * @return PaginationInterface<string>
     *
     * @throws Exception
     */
    public function fetch(Query $query): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'c.id',
                'c.avatar',
                'c.text',
                'c.created_at',
            )
            ->from('comments', 'c')
            ->andWhere('c.parent_id is null');

        if (!in_array($query->sort, ['id', 'created_at'], true)) {
            throw new \UnexpectedValueException('Cannot sort by '.$query->sort);
        }

        $qb->orderBy($query->sort, 'desc' === $query->direction ? 'desc' : 'asc');

        $pagination = $this->paginator->paginate($qb, $query->page, $query->size);
        $comments = (array) $pagination->getItems();
        $answers = $this->batchLoadAnswers(array_column($comments, 'id'));

        $pagination->setItems(
            array_map(static function (array $comment) use ($answers) {
                return array_merge($comment, [
                    'answers' => array_filter($answers, static function (array $answer) use ($comment) {
                        return $answer['parent_id'] === $comment['id'];
                    }),
                ]);
            }, $comments)
        );

        return $pagination;
    }

    /**
     * @param array<int> $ids
     *
     * @return array<int, array<string,mixed>>
     *
     * @throws Exception
     */
    private function batchLoadAnswers(array $ids): array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'a.id',
                'a.parent_id',
                'a.avatar',
                'a.text',
                'a.created_at'
            )
            ->from('comments', 'a')
            ->andWhere('a.is_visible = true and a.parent_id IN (:parent)')
            ->orderBy('created_at', 'desc')
            ->setParameter('parent', $ids, Connection::PARAM_INT_ARRAY)
            ->executeQuery();

        return $stmt->fetchAllAssociative();
    }
}
