<?php

namespace App\Comment\Query\GetAllComments;

class Query
{
    public function __construct(
        public int $page,
        public int $size,
        public string $sort,
        public string $direction
    ) {
    }
}
