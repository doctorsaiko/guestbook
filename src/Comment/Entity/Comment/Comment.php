<?php

namespace App\Comment\Entity\Comment;

use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'comments')]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'comment_comment_email', nullable: true)]
    private ?Email $email = null;

    #[ORM\Column(type: 'text')]
    private string $text;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $avatar = null;

    #[ORM\Column(type: 'boolean')]
    private bool $isVisible = true;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $createdAt;

    #[ORM\Version]
    #[ORM\Column(type: 'integer')]
    private int $version;

    /**
     * @var Collection<int, self>
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: Comment::class, fetch: 'EXTRA_LAZY', orphanRemoval: true)]
    private Collection $children;

    #[ORM\ManyToOne(targetEntity: Comment::class, inversedBy: 'children')]
    #[ORM\JoinColumn(name: 'parent_id', referencedColumnName: 'id')]
    private ?Comment $parent = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $lastNotificationSendAt = null;

    private function __construct(
        string $text,
    ) {
        $this->text = $text;
        $this->createdAt = new DateTimeImmutable();
        $this->children = new ArrayCollection();
    }

    public static function createComment(
        string $text,
        ?Email $email = null,
        ?string $avatar = null,
    ): self {
        $comment = new self($text);
        $comment->email = $email;
        $comment->avatar = $avatar;

        return $comment;
    }

    public static function createAnswer(
        string $text,
        ?Email $email = null,
        ?string $avatar = null,
        ?self $parent = null
    ): self {
        $answer = new self($text);
        $answer->email = $email;
        $answer->avatar = $avatar;
        $answer->parent = $parent;

        if (!$parent->isVisible()) {
            throw new \DomainException('sorry, comment is hidden!');
        }
        if ($parent->getParent()) {
            throw new \DomainException('you cannot make answer to answer, please use citation!');
        }

        return $answer;
    }

    public function setIsVisible(bool $isVisible): void
    {
        $this->isVisible = $isVisible;
    }

    public function setLastNotificationSendAt(): void
    {
        $this->lastNotificationSendAt = new DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function getParent(): ?Comment
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection<int, self>
     */
    public function getChildren(): ArrayCollection
    {
        return $this->children;
    }

    public function isVisible(): bool
    {
        return $this->isVisible;
    }

    public function getLastNotificationSendAt(): ?DateTimeImmutable
    {
        return $this->lastNotificationSendAt;
    }
}
