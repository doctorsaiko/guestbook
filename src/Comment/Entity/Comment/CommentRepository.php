<?php

namespace App\Comment\Entity\Comment;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class CommentRepository
{
    /**
     * @var EntityRepository<Comment>
     */
    private EntityRepository $repo;

    public function __construct(private EntityManagerInterface $em)
    {
        $this->repo = $this->em->getRepository(Comment::class);
    }

    public function getById(int $id): Comment
    {
        $comment = $this->repo->find($id);

        if (!$comment) {
            throw new \DomainException('Comment not found!');
        }

        return $comment;
    }
}
