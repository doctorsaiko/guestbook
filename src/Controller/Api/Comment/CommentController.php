<?php

namespace App\Controller\Api\Comment;

use App\Comment\UseCase\AddAnswer;
use App\Comment\UseCase\AddComment;
use App\Controller\Api\Response\CreatedResponse;
use App\Controller\Api\Validator;
use App\Service\Uploader\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CommentController extends AbstractController
{
    public function __construct(
        private SerializerInterface $serializer,
        private Validator $validator
    ) {
    }

    #[Route('/api/comment', name: 'api_comment_create', methods: 'POST')]
    public function create(
        Request $request,
        AddComment\Handler $handler,
        AddComment\Command $command,
        CreatedResponse $response,
        FileUploader $uploader
    ): JsonResponse {
        if ($text = $request->get('text')) {
            $command->text = $text;
        }

        $command->avatar = $request->files->get('avatar');

        $this->validator->validate($command);

        if ($command->avatar) {
            $avatar = $uploader->upload($request->files->get('avatar'));
            $command->avatarPath = $avatar->getPath();
        }

        $handler->handle($command);

        return $response->toJson();
    }

    #[Route('/api/comment/{id}/answer', name: 'api_comment_answer_add', methods: 'POST')]
    public function addAnswer(
        int $id,
        Request $request,
        AddAnswer\Handler $handler,
        AddAnswer\Command $command,
        CreatedResponse $response,
        FileUploader $uploader
    ): JsonResponse {
        if ($text = $request->get('text')) {
            $command->text = $text;
        }

        $command->avatar = $request->files->get('avatar');
        $command->parentId = $id;

        $this->validator->validate($command);

        if ($command->avatar) {
            $avatar = $uploader->upload($request->files->get('avatar'));
            $command->avatarPath = $avatar->getPath();
        }

        $handler->handle($command);

        return $response->toJson();
    }
}
