<?php

namespace App\Controller\Api\Response;

use App\Controller\Api\PaginationSerializer;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

class CommentWithAnswersResponse extends ApiResponse
{
    /**
     * @var PaginationInterface<string|int|array>
     */
    private PaginationInterface $pagination;

    /**
     * @param PaginationInterface<string|int|array> $pagination
     * @return $this
     */
    public function setComments(PaginationInterface $pagination): self
    {
        $this->pagination = $pagination;

        return $this;
    }

    public function toJson(): JsonResponse
    {
        return $this->json([
            'items' => array_map(
                static function (array $item) {
                    return [
                        'id' => $item['id'],
                        'avatar' => $item['avatar'] ?? null,
                        'text' => $item['text'],
                        'created_at' => $item['created_at'],
                        'answers' => $item['answers'],
                    ];
                },
                (array) $this->pagination->getItems()
            ),
            'pagination' => PaginationSerializer::toArray($this->pagination),
        ]);
    }
}
