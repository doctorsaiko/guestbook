<?php

namespace App\Controller\Api\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

abstract class ApiResponse
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    abstract public function toJson(): JsonResponse;

    protected function json(
        mixed $data,
        int $status = 200,
        array $headers = [],
        array $context = []
    ): JsonResponse {
        $json = $this->serializer->serialize(
            $data,
            'json',
            array_merge([
                'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS,
            ], $context)
        );

        return new JsonResponse($json, $status, $headers, true);
    }
}
