<?php

namespace App\Controller\Api\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class CreatedResponse extends ApiResponse
{
    public function toJson(): JsonResponse
    {
        return $this->json(['message' => 'ok'], 201);
    }
}
