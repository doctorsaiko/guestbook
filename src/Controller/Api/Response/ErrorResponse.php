<?php

namespace App\Controller\Api\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorResponse extends ApiResponse
{
    private int $code = 400;
    /**
     * @var array<string>|string
     */
    private array|string $message;

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param array<string>|string $message
     *
     * @return $this
     */
    public function setMessage(array|string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function toJson(): JsonResponse
    {
        $response = [
            'error' => [
                'code' => $this->code,
            ],
        ];

        if (is_array($this->message)) {
            $response['error']['messages'] = $this->message;
        } else {
            $response['error']['message'] = $this->message;
        }

        return $this->json($response, 400);
    }
}
