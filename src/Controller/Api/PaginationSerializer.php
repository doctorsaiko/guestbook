<?php

namespace App\Controller\Api;

use Knp\Component\Pager\Pagination\PaginationInterface;

final class PaginationSerializer
{
    /**
     * @param PaginationInterface<string> $pagination
     * @return array<string, float|int>
     */
    public static function toArray(PaginationInterface $pagination): array
    {
        return [
            'count' => $pagination->count(),
            'total' => $pagination->getTotalItemCount(),
            'per_page' => $pagination->getItemNumberPerPage(),
            'page' => $pagination->getCurrentPageNumber(),
            'pages' => ceil($pagination->getTotalItemCount() / $pagination->getItemNumberPerPage()),
        ];
    }
}
