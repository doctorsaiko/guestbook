<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\Api\Exception\ValidationException;
use App\Controller\Api\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionFormatter implements EventSubscriberInterface
{
    public function __construct(private ErrorResponse $response)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $request = $event->getRequest();

        if (!str_starts_with($request->attributes->get('_route', ''), 'api_')) {
            return;
        }

        if ($exception instanceof \DomainException) {
            $this->response->setMessage($exception->getMessage());
            $event->setResponse($this->response->toJson());

            return;
        }

        if ($exception instanceof ValidationException) {
            $this->response->setMessage($exception->getViolations());
            $this->response->setCode(422);
            $event->setResponse($this->response->toJson());

            return;
        }
    }
}
