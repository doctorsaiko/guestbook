<?php

namespace App\Controller\Api\Home;

use App\Comment\Query\GetAllComments;
use App\Controller\Api\Response\CommentWithAnswersResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public const PER_PAGE = 50;

    #[Route('/api', name: 'api_home')]
    public function index(
        Request $request,
        GetAllComments\Fetcher $fetcher,
        CommentWithAnswersResponse $response
    ): JsonResponse {
        $query = new GetAllComments\Query(
            page: $request->query->getInt('page', 1),
            size: self::PER_PAGE,
            sort: $request->query->get('sort', 'id'),
            direction: $request->query->get('size', 'desc'),
        );

        $response->setComments($fetcher->fetch($query));

        return $response->toJson();
    }
}
