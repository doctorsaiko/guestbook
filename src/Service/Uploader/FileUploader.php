<?php

namespace App\Service\Uploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Uid\Uuid;

class FileUploader
{
    public function __construct(
        private string $baseUrl,
        private string $uploadPath,
        private string $urlPath
    ) {
    }

    public function upload(UploadedFile $file): File
    {
        $name = Uuid::v4()->toRfc4122().'.'.$file->getClientOriginalExtension();

        $size = $file->getSize();
        $file->move($this->uploadPath, $name);

        $path = $this->baseUrl.$this->urlPath.'/'.$name;

        return new File($path, $name, $size);
    }
}
