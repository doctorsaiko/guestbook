<?php

namespace App\Auth\Entity\User;

use Doctrine\ORM\Mapping as Orm;

#[ORM\Entity]
#[ORM\Table('users')]
class User
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    #[Orm\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'auth_user_email', unique: true, nullable: true)]
    private ?Email $email = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $password = null;

    /**
     * @var array<string>
     */
    #[ORM\Column(type: 'json')]
    private array $roles = [self::ROLE_USER];

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $emailVerifiedAt = null;

    #[ORM\Version]
    #[ORM\Column(type: 'integer')]
    private int $version;

    public function __construct(Email $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function addRole(string $role): void
    {
        $roles = $this->roles;

        if (!in_array($role, [self::ROLE_ADMIN, self::ROLE_USER])) {
            throw new \DomainException('role not exist');
        }

        $roles[] = $role;

        $this->roles = array_unique($roles);
    }

    public function makeEmailVerified(): void
    {
        $this->emailVerifiedAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return array<string>
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getEmailVerifiedAt(): ?\DateTimeImmutable
    {
        return $this->emailVerifiedAt;
    }

    public function getVersion(): int
    {
        return $this->version;
    }
}
