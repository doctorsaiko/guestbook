<?php

namespace App\Auth\UseCase\MakeSuperAdmin;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    #[Assert\Email]
    #[Assert\NotBlank]
    #[Assert\Length(max: 250)]
    public string $email;

    #[Assert\NotBlank]
    #[Assert\Length(min: 8, max: 14)]
    public string $password;
}