<?php

namespace App\Auth\UseCase\MakeSuperAdmin;

use App\Auth\Entity\User\Email;
use App\Auth\Entity\User\User;
use App\Auth\Entity\User\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\NativePasswordHasher;

class Handler
{
    public function __construct(
        private EntityManagerInterface $em,
        private UserRepository $userRepository,
        private NativePasswordHasher $hasher
    ) {
    }

    public function handle(Command $command): void
    {
        $email = new Email($command->email);

        if ($this->userRepository->hasByEmail($email)) {
            throw new \DomainException('email has been taken');
        }

        $password = $this->hasher->hash($command->password);

        $user = new User($email, $password);
        $user->addRole(User::ROLE_ADMIN);
        $user->makeEmailVerified();

        $this->em->persist($user);
        $this->em->flush();
    }
}
