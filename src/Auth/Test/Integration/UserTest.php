<?php

namespace App\Auth\Test\Integration;

use App\Auth\Entity\User\User;
use App\Auth\Entity\User\UserRepository;
use App\Auth\UseCase\MakeSuperAdmin;
use App\Tests\Integration\IntegrationTestCase;
use Symfony\Component\PasswordHasher\Hasher\NativePasswordHasher;

class UserTest extends IntegrationTestCase
{
    private UserRepository $userRepository;
    private NativePasswordHasher $hasher;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = self::$container->get(UserRepository::class);
        $this->hasher = self::$container->get(NativePasswordHasher::class);
    }

    public function testMakeSuperAdmin(): void
    {
        $command = new MakeSuperAdmin\Command();

        $email = $command->email = 'test@email.com';
        $password = $command->password = '12345678';

        $handler = self::$container->get(MakeSuperAdmin\Handler::class);
        $handler->handle($command);

        $id = $this->em->getConnection()->lastInsertId('users_id_seq');
        $user = $this->userRepository->getById($id);

        self::assertEquals($email, $user->getEmail()->getValue());
        self::assertTrue($this->hasher->verify($user->getPassword(), $password));
        self::assertContains(User::ROLE_ADMIN, $user->getRoles());
        self::assertNotNull($user->getEmailVerifiedAt());
    }
}
