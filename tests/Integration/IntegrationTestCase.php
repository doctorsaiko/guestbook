<?php

namespace App\Tests\Integration;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IntegrationTestCase extends KernelTestCase
{
    protected ?EntityManager $em;
    protected static ContainerInterface $container;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        self::$container = $kernel->getContainer();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->em->getConnection()->beginTransaction();
    }

    public function tearDown(): void
    {
        $this->em->getConnection()->rollBack();
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }
}
